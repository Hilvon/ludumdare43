﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIText = UnityEngine.UI.Text;
using UIImage = UnityEngine.UI.Image;

public class EffectDisplay : MonoBehaviour {
    public Sprite DamageSprite;
    public Sprite HealSprite;
    public UIImage EffectIcon;
    public UIText EffectNumber;

	public void DisplayEffect(bool IsDamage, int Type, int Amount)
    {
        EffectIcon.color = IsDamage ? HealthBar.GetColorPyType(Type) : Color.green;
        EffectIcon.sprite = IsDamage ? DamageSprite : HealSprite;
        EffectNumber.text = "" + Amount;

    }
}
