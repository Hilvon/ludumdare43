﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIText = UnityEngine.UI.Text;

public class UITextMirror : MonoBehaviour {
    public UIText baseText;
    UIText myText;
	// Use this for initialization
	void Start () {
        myText = GetComponent<UIText>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        myText.text = baseText.text;
	}
}
