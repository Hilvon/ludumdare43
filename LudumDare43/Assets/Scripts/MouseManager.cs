﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour {
    public static MouseManager main;
	// Use this for initialization
	void Start () {
        if (main == null) main = this;
	}

    IHover LastHoverObject;
    IDragable currentDraggedObject;

    ILeftClickable LMBDownTarget;
    IRightClickable RMBDownTarget;

    public LayerMask InteractableLayers;
	// Update is called once per frame
	void Update () {
        Ray curMouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitinfo;
        Collider mouseTarget = null;
        if (Physics.Raycast(curMouseRay,out hitinfo, 999, InteractableLayers)) {
            mouseTarget = hitinfo.collider;
        }

        //HandlingHovers
        IHover curHover = GetTargetComponent<IHover>(mouseTarget);
        if (curHover!= LastHoverObject)
        {
            if (LastHoverObject != null) LastHoverObject.OnMouseExit();
            if (curHover != null) curHover.OnMouseEnter();
            LastHoverObject = curHover;
        }

        if (currentDraggedObject != null)
        {
            currentDraggedObject.OnDrag(hitinfo, mouseTarget != null);
        }

        // if Mouse Buttom Up - stop Drag, or confirm left click.
        if (Input.GetMouseButtonUp(0)) {

            if (currentDraggedObject != null) {
                currentDraggedObject.OnDragConfirm();
                currentDraggedObject = null;
            }
            if (LMBDownTarget != null)
            {
                ILeftClickable endTarget = GetTargetComponent<ILeftClickable>(mouseTarget);
                if (endTarget == LMBDownTarget) LMBDownTarget.OnClicked();
            }
        }
        if (Input.GetMouseButtonUp(1))
        {
            if (currentDraggedObject != null)
            {
                currentDraggedObject.OnDragCancel();
                currentDraggedObject = null;
            }
            if (RMBDownTarget != null)
            {
                IRightClickable endTarget = GetTargetComponent<IRightClickable>(mouseTarget);
                if (endTarget == RMBDownTarget) RMBDownTarget.OnClicked();
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            IDragable testDrag = GetTargetComponent<IDragable>(mouseTarget);
            if (testDrag != null && testDrag.OnDragStart())
            {

                currentDraggedObject = testDrag;
            }

            LMBDownTarget = GetTargetComponent<ILeftClickable>(mouseTarget);
        }

        if (Input.GetMouseButtonDown(1))
        {
            RMBDownTarget = GetTargetComponent<IRightClickable>(mouseTarget);
        }


    }

    T GetTargetComponent<T>(Collider target) where T:class
    {
        if (target == null) return null;
        return target.GetComponentInParent<T>();
        //T[] tmp = target.GetComponentsInParent<T>(false);
        //for (int i = 0; i < tmp.Length ; i++)
        //{
        //    MonoBehaviour C = tmp[i] as MonoBehaviour;
        //    if (C != null && C.enabled) return tmp[i];
        //}
        //return null;
    }

    public interface IHover
    {
        void OnMouseEnter();
        void OnMouseExit();
    }
    public interface IDragable
    {
        bool OnDragStart();
        void OnDrag(RaycastHit HitInfo, bool isHit);
        void OnDragConfirm();
        void OnDragCancel();
    }

    public interface ILeftClickable
    {
        void OnClicked();
    }
    public interface IRightClickable
    {
        void OnClicked();
    }
    //[System.Serializable]
    //struct InteractionMode
    //{
    //    string Label;
    //    LayerMask Mask;
    //}
}
