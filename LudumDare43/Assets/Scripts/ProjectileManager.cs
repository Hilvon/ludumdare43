﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour {
    public Projectile Cian, Magenta, Yellow, Heal;
	public Projectile GetProjectile(bool isHEal, int Type)
    {
        return isHEal ? Heal : Type == 0 ? Cian : Type == 1 ? Magenta : Yellow;
    }
}
