﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Hex : HarmableTableObject, MouseManager.IRightClickable, MouseManager.IHover
{
    public static void ClearList()
    {
        HexesInPlay.Clear();
    }
    static List<Hex> HexesInPlay = new List<Hex>();
    public static void IterateHexes(System.Action<Hex> Callback)
    {
        HexesInPlay.OrderBy((H) => { return Random.Range(0f, 1f); }).Where((H) => { Callback(H); return false; }).ToArray();
    }
    public static void HighlightTargetHex(int Type)
    {
        HexesInPlay.Where((H) => { H.IsValidTarget = false; H.HighlightColor = Color.clear; return false; }).ToArray();
        IEnumerable<HarmableTableObject> res = HexesInPlay.Where((H) => { return H.HPType == Type && H.IsDefender; }).Cast<HarmableTableObject>();
        if (!res.Any())
        {
            res = HexesInPlay.Where((H) => { return H.HPType == Type; }).Cast<HarmableTableObject>().Union(new List<HarmableTableObject>() { CentralModule.main.EnemyHealth });
        }
        Color HC = HealthBar.GetColorPyType(Type);
        res.Where((H) => { H.IsValidTarget = true; H.HighlightColor = HC; return true; }).ToArray();
    }
    static HarmableTableObject GetHealTargetHex()
    {
        return HexesInPlay.Cast<HarmableTableObject>().Where((H) => { return !H.IsMaxHP(); }).OrderBy((H) => { return Random.Range(0f, 1f); }).FirstOrDefault();
    }

    public bool IsDamageEffect = true;
    public int EffectType;
    public int EffectStrength;
    public int EffectCooldown;
    public int InitialCooldown;

    int curSteps;

    public EffectDisplay EffectDisplay;
    public HexTimeDisplay TimerDislay;
    public HealthBar Costs;
    public BurningEffect Burning;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        Costs.Display(0, 0, 0);
        Costs.InitHealth(MaxHP, HPType, IsDefender);
        OnHPChanged += Costs.UpdateHealth;
        curSteps = InitialCooldown;
        EffectDisplay.DisplayEffect(IsDamageEffect, EffectType, EffectStrength);
        TimerDislay.InitDisplay(EffectCooldown, InitialCooldown);

        HexesInPlay.Add(this);

        OnHPChanged += Costs.UpdateHealth;
        OnDead += CardKilled;

        Initialize();
    }
    void CardKilled()
    {
        Debug.Log("CardKelled");
        HexesInPlay.Remove(this);
        Costs.UpdateHealth(0);
        Burning.SetBurnIntencity(200, () =>
        {
            myRenderer.enabled = false;
            myUI.gameObject.SetActive(false);
            Burning.SetBurnIntencity(0, () =>
            {
                gameObject.SetActive(false);
                myRenderer.enabled = true;
                myUI.gameObject.SetActive(true);

            }, 1);
        });
    }


    // Update is called once per frame


    public void RunAction(System.Action Callback)
    {
        Debug.Log("Running Action: "+curSteps+" ; "+name);
        //This should select appropriate target for effect, execute it, and then run CallBack when done;
        if (curSteps == 0)
        {
            // ...
            if (IsDamageEffect)
            {
                HarmableTableObject HO = CentralModule.main.PlayArea.GetTarget(EffectType);
                if (HO == null) HO = CentralModule.main.PlayerHealth;
                CentralModule.main.ProjectileManager.GetProjectile(false, EffectType).Launch(transform.position + Vector3.up * 0.5f, HO.transform.position, () =>
                {
                    HO.Damage(EffectStrength);
                    Callback();
                });
            }
            else
            {
                HarmableTableObject HO = GetHealTargetHex();
                if (HO == null) HO = CentralModule.main.EnemyHealth;

                CentralModule.main.ProjectileManager.Heal.Launch(transform.position + Vector3.up * 0.5f, HO.transform.position, () =>
                {
                    HO.Repair(EffectStrength);
                    Callback();
                });
            }

            curSteps += EffectCooldown;
        }
        else
        {
            curSteps -= 1;
            Callback();
        }
        TimerDislay.UpdateCurStep(curSteps);
    }

    bool IsInspected;
    public void OnClicked()
    {
        IsInspected = true;
        OverrideCardPos = true;

        SetCardGraphicPos(CentralModule.main.InspectionPos.position, CentralModule.main.InspectionPos.forward * -1);

        //if (InPlay && EnoughResources)
        //{
        //    //We can do something here;
        //}
    }
    public static Hex ActiveHex;
    public void OnMouseEnter()
    {
        ActiveHex = this;
    }
    public void OnMouseExit()
    {
        ActiveHex = null;
        if (IsInspected)
        {
            OverrideCardPos = false;
        }
        //HoverState = false;
        //if (OnHoverStateChanged != null) OnHoverStateChanged();
    }
}
