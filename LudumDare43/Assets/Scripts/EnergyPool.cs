﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPool : MonoBehaviour {

    public int Red, Green, Blue;

    public EnergyCoin  RedTemplate, GreenTemplate, BlueTemplate;
    public Transform RPos, GPos, BPos; 

    public void Award(int Red, int Green, int Blue)
    {
        this.Red += Red;
        this.Green += Green;
        this.Blue += Blue;
        //Need to spawn/Activate more coins in the Sacrifice spot;
        while (RedCoins.Count < this.Red)
        {
            RedCoins.Add(Instantiate(RedTemplate, CentralModule.main.SacrificeSpot.transform.position, Quaternion.identity, transform));
        }
        while (BlueCoins.Count < this.Blue)
        {
            BlueCoins.Add(Instantiate(BlueTemplate, CentralModule.main.SacrificeSpot.transform.position, Quaternion.identity, transform));
        }
        while (GreenCoins.Count < this.Green)
        {
            GreenCoins.Add(Instantiate(GreenTemplate, CentralModule.main.SacrificeSpot.transform.position, Quaternion.identity, transform));
        }
    }
    public bool CanTap(int Red, int Green, int Blue)
    {
        return this.Red >= Red && this.Green >= Green && this.Blue >= Blue;
    }
    public void Tap(int Red, int Green, int Blue, Vector3 Pos)
    {
        if (CanTap(Red,Green, Blue))
        {
            TRed = Red;
            TGreen = Green;
            TBlue = Blue;
            TPos = Pos;
        }
    }
    public void UseTapped()
    {
        if (TRed > 0) SetOff(RedCoins, Red, TRed);
        if (TGreen > 0) SetOff(GreenCoins, Green, TGreen);
        if (TBlue > 0) SetOff(BlueCoins, Blue, TBlue);

        Red -= TRed;
        Green -= TGreen;
        Blue -= TBlue;
        TRed = 0;
        TGreen = 0;
        TBlue = 0;
    }
    void SetOff(List<EnergyCoin> List, int Cnt, int TCnt)
    {
        for (int i = Cnt-TCnt; i < Cnt ; i++)
        {
            EnergyCoin curCoin = List[i];
            Instantiate(curCoin.Explosion, curCoin.transform.position, Quaternion.identity);
        }
    }
    System.Action callback;
    bool Burning;
    float nextBurn;
    public float BurnFreq;
    public void BurnMana(System.Action Callback) {
        callback+= Callback;
        Burning = true;
        nextBurn = 0;
    }
    int TRed, TGreen, TBlue;
    Vector3 TPos;
    List<EnergyCoin> RedCoins = new List<EnergyCoin>();
    List<EnergyCoin> GreenCoins = new List<EnergyCoin>();
    List<EnergyCoin> BlueCoins = new List<EnergyCoin>();
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Burning)
        {
            nextBurn -= Time.deltaTime;
            if (nextBurn<=0)
            {
                int burned=0;
                if (Red>0)
                {
                    SetOff(RedCoins, Red, 1);
                    burned++;
                    Red--;
                }
                if (Blue >0)
                {
                    SetOff(BlueCoins, Blue, 1);
                    burned++;
                    Blue--;
                }
                if (Green > 0)
                {
                    SetOff(GreenCoins, Green, 1);
                    burned++;
                    Green--;
                }
                if (burned > 0)
                {
                    CentralModule.main.PlayerHealth.Damage(burned);
                    nextBurn += BurnFreq;
                }
                else
                {
                    System.Action tmp = callback;
                    Burning = false;
                    callback = null;
                    if (tmp!=null) tmp();
                }
            }
        }
        else
        {
            int TNum = UpdateCoins(RedCoins, RPos, Red, TRed, 0);
            TNum = UpdateCoins(GreenCoins, GPos, Green, TGreen, TNum);
            UpdateCoins(BlueCoins, BPos, Blue, TBlue, TNum);
        }
    }
    int UpdateCoins(List<EnergyCoin> List, Transform Pos, int Cnt, int TCnt, int tNum)
    {
        int TNum = tNum;
        for (int i = 0; i < List.Count; i++)
        {
            EnergyCoin curCoin = List[i];
            if (i < Cnt)
            {
                Vector3 reqPosition;
                if (i < Cnt - TCnt)
                {
                    reqPosition = Pos.position + Pos.up * (i * 0.3f + 0.1f);
                }
                else
                {
                    reqPosition = TPos + Vector3.up * (TNum * 0.3f + 0.1f);
                    TNum++;
                }
                curCoin.gameObject.SetActive(true);
                curCoin.transform.position = Vector3.Lerp(curCoin.transform.position, reqPosition, 4 * Time.deltaTime);
            }
            else
            {
                curCoin.gameObject.SetActive(false);
                curCoin.transform.position = CentralModule.main.SacrificeSpot.transform.position;
            }
        }
        return TNum;
    }
}
