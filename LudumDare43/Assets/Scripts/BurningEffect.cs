﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningEffect : MonoBehaviour {
    public ParticleSystem myParticleSystem;
    ParticleSystem.EmissionModule curEmission;
    ParticleSystem.MainModule curMainParticles;

    System.Action OnIntencityChanged;
    int TargetIntencity;
    float Delay;
    public void SetBurnIntencity(int N, System.Action Callback = null, float Wait=0)
    {
        TargetIntencity = N;
        if (Callback != null) OnIntencityChanged += Callback;
        if (TargetIntencity > 0) gameObject.SetActive(true);
        Delay = Wait;
    }
    public float FireUpSpeed = 3;
    // Use this for initialization
    void Start () {
        myParticleSystem = GetComponentInChildren<ParticleSystem>();
        curEmission = myParticleSystem.emission;
        curMainParticles = myParticleSystem.main;
    }
	
	// Update is called once per frame
	void Update () {
        ParticleSystem.MinMaxCurve RoTc = curEmission.rateOverTime;
        //float curRate = curEmission.rateOverTime.constant;
        RoTc.constant = Mathf.Lerp(RoTc.constant, TargetIntencity, FireUpSpeed * Time.deltaTime);

        curEmission.rateOverTime = RoTc;
        curMainParticles.startSize = Mathf.Log(RoTc.constant + 1, 10);
        if (Mathf.Abs(RoTc.constant - TargetIntencity) < 1 && OnIntencityChanged != null)
        {
            if (Delay > 0)
            {
                Delay -= Time.deltaTime;
            }
            else
            {
                System.Action tmp = OnIntencityChanged;
                OnIntencityChanged = null;

                if (TargetIntencity == 0) gameObject.SetActive(false);
                tmp();
            }
        }
    }
}
