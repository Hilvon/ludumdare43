﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    public float TravelSpeed;
    public Explosion Explosion;
	// Use this for initialization
	void Start () {
		
	}
    public void Launch(Vector3 From, Vector3 To, System.Action Callback, float Wait = 1)
    {
        OnConcealed += () =>
        {
            transform.position = From;
            TargetPos = To;
            this.Callback = Callback;
            gameObject.SetActive(true);
            Timer = Wait;
            ExplosionSpawned = false;
            OnConcealed = null;
        };
        if (Timer <= 0) OnConcealed();
    }
    float Timer;
    Vector3 TargetPos;
    bool ExplosionSpawned;
    // Update is called once per frame
    System.Action Callback;
    System.Action OnConcealed;
	void Update () {
        transform.rotation = Quaternion.LookRotation(TargetPos - transform.position, Vector3.up);
        transform.position = Vector3.MoveTowards(transform.position, TargetPos, TravelSpeed);
        if (Vector3.Distance(transform.position,TargetPos)<0.5f)
        {
            if (!ExplosionSpawned)
            {
                Instantiate(Explosion, TargetPos, Quaternion.identity);
                System.Action tmp = Callback;
                Callback = null;
                tmp();
                ExplosionSpawned = true;
            }
            if (Timer > 0) { Timer -= Time.deltaTime; }
            else
            {

                gameObject.SetActive(false);
                if (OnConcealed != null) OnConcealed();
            }
        }
    }
}
