﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Hand : MonoBehaviour {
    public static Hand main;

    public Transform LeftSide;
    public Transform RightSide;

    public float leftOffset, rightOffset, downOffset, zOffset;

    public Card[] HeldCards;
	// Use this for initialization
	void Start () {
        if (main == null) main = this;
        EvalCurve();

    }

    public bool IsCardInHand(Card Card)
    {
        foreach (Card C in HeldCards)
        {
            if (C == Card) return true;
        }
        return false;
    }

    public void AddCards(Card[] Cards)
    {
        List<Card> newList = new List<Card>(HeldCards);
        newList.AddRange(Cards);
        HeldCards = newList.ToArray();
    }
    public void LoseCard(Card C)
    {
        List<Card> newList = new List<Card>();
        foreach(Card c in HeldCards)
        {
            if (c != C) newList.Add(c);
        }
        HeldCards = newList.ToArray();
    }
    void EvalCurve()
    {
        Ray tmpRay;
        tmpRay = Camera.main.ViewportPointToRay(new Vector2(leftOffset, downOffset));
        LeftSide.position = tmpRay.GetPoint(zOffset);
        LeftSide.rotation = Quaternion.LookRotation(tmpRay.direction, Camera.main.transform.up);

        tmpRay = Camera.main.ViewportPointToRay(new Vector2(1-rightOffset, downOffset));
        RightSide.position = tmpRay.GetPoint(zOffset);
        RightSide.rotation = Quaternion.LookRotation(tmpRay.direction, Camera.main.transform.up);


        CardCurve = new TransformSpline(LeftSide, RightSide);
    }
    int prevHeldCards;

    TransformSpline CardCurve;
	// Update is called once per frame
	void Update () {
        EvalCurve(); // This is temprary for testing purpose. Remove from live to save computations.


        float Breaks = HeldCards.Length + 1;
        for (int i = 0; i < HeldCards.Length; i++)
        {
            float t = (i+1) / Breaks;
            Card curCard = HeldCards[i];
            curCard.SetCardPos ( CardCurve.Position(t),  CardCurve.Rotation(t));
        }
    }


    class CubeSpline
    {
        float A, B, C, D;
        public float Eval(float T)
        {
            return ((A * T + B) * T + C) * T + D;
        }
        public float Vel(float T)
        {
            return (3 * A * T + 2 * B) * T + C;
        }
        public CubeSpline(float P0, float V0, float P1, float V1)
        {
            D = P0;
            C = V0;
            A = 2 * (P0 - P1) + V1 + V0;
            B = P1 - P0 - V0 - A;
        }
    }
    class Vector3Spline
    {
        CubeSpline X, Y, Z;
        public Vector3 Eval(float T)
        {
            return new Vector3(X.Eval(T), Y.Eval(T), Z.Eval(T));
        }
        public Vector3 Velocity(float T)
        {
            return new Vector3(X.Vel(T), Y.Vel(T), Z.Vel(T));
        }
        public Vector3Spline(Vector3 P0, Vector3 V0, Vector3 P1, Vector3 V1)
        {
            X = new CubeSpline(P0.x, V0.x, P1.x, V1.x);
            Y = new CubeSpline(P0.y, V0.y, P1.y, V1.y);
            Z = new CubeSpline(P0.z, V0.z, P1.z, V1.z);
        }
    }

    class TransformSpline
    {
        Vector3Spline P, U;
        Quaternion L, R;
        Quaternion FtR = Quaternion.Euler(0,0,0);
        public TransformSpline(Transform A, Transform B)
        {
            float dist = Vector3.Distance(A.position, B.position);
            P = new Vector3Spline(A.position, A.right * dist, B.position, B.right * dist);
            U = new Vector3Spline(Vector3.zero, A.up, Vector3.up * 10 * Vector3.Dot(Vector3.up, A.up), B.up);
            L = A.rotation;
            R = B.rotation;
        }

        public Vector3 Position(float T)
        {
            return P.Eval(T);
        }
        public Quaternion Rotation(float T)
        {
            //return FtR * Quaternion.LookRotation(P.Velocity(T), U.Velocity(T));
            return Quaternion.LerpUnclamped(L, R, T);
        }
    }
}
