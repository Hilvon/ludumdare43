﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayArea : MonoBehaviour {
    List<Card> CardsInPlay = new List<Card>();
	public void AddCard(Card C)
    {
        CardsInPlay.Add(C);
    }
    public void LoseCard(Card C)
    {
        CardsInPlay.Remove(C);
    }
    public bool IsCardInPlay(Card C)
    {
        foreach (Card c in CardsInPlay)
        {
            if (c == C) return true;
        }
        return false;
    }
    public void HighlightHealTargets()
    {
        CardsInPlay.Cast<HarmableTableObject>().Union(new HarmableTableObject[] { CentralModule.main.PlayerHealth }).Where((H) => { H.HighlightColor = H.IsMaxHP() ? Color.yellow : Color.green; H.IsValidTarget = true; return true; }).ToArray();
    }
    public HarmableTableObject GetTarget(int Type)
    {
        IEnumerable<HarmableTableObject> res = CardsInPlay.Cast<HarmableTableObject>().Where((c) => { return c.HPType == Type && c.IsDefender; });
        if (!res.Any()) res.Union(CardsInPlay.Cast<HarmableTableObject>()).Union(new HarmableTableObject[] { CentralModule.main.PlayerHealth });
        return res.OrderBy((C) => { return Random.Range(0f, 1f); }).FirstOrDefault();
    }

    // Update is called once per frame
    void Update () {
		
	}

    MaterialPropertyBlock myPRP;
    public Color HighlightColor
    {
        get { return myPRP.GetColor("_GlowColor"); }
        set
        {
            myPRP.SetColor("_GlowColor", value);
            myRenderer.SetPropertyBlock(myPRP);
        }
    }
    MeshRenderer myRenderer;
    // Use this for initialization
    void Start()
    {
        myRenderer = GetComponentInChildren<MeshRenderer>();
        myPRP = new MaterialPropertyBlock();
    }
}
