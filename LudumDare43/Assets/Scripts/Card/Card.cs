﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : HarmableTableObject, MouseManager.IHover, MouseManager.IDragable , MouseManager.IRightClickable {
    public static event System.Action OnCardDragStart;
    public static event System.Action OnCardDragStop;
    public static Card ActiveCard;
    public static void Clear()
    {
        Pile = new CardPiles();
    }

    public string Title;
    public int RedCost, GreenCost, BlueCost, RedSalvade, GreenSalvage, BlueSalvage, RedActivation, BlueActivation, GreenActivation, EffectType, EffectStrength;

    public CardCostDisplay Salvage;
    public CardCostDisplay Activation;
    public CardCostDisplay BAckSalvage;
    public EffectDisplay Effect;
    public HealthBar Costs;
    public UnityEngine.UI.Text Label;

    static CardPiles Pile = new CardPiles();
    public static void DiscadCard(Card C)
    {
        Pile.Discard(C);
    }
    public static Card DrawCard()
    {
        return Pile.Draw();
    }

    //private float PositionTransferProgress;
    //private float ViewCangeProgress;

    public BurningEffect Burning;
    // Use this for initialization
    protected override void Start() {
        base.Start();
        OnHPChanged += Costs.UpdateHealth;
        OnDead += CardKilled;
    }
    public override void  Initialize()
    {
        base.Initialize();
        Costs.ShowCosts();
    }

    public void UpdateDisplays()
    {
        Label.text = Title;
        Costs.Display(RedCost, GreenCost, BlueCost);
        Salvage.Display(RedSalvade, GreenSalvage, BlueSalvage);
        BAckSalvage.Display(RedSalvade, GreenSalvage, BlueSalvage);
        Costs.InitHealth(MaxHP, HPType, IsDefender);
        Activation.Display(RedActivation, GreenActivation, BlueActivation);
        Effect.DisplayEffect(EffectType != -1, EffectType, EffectStrength);
    }

    void CardKilled()
    {
        Debug.Log("CardKelled");
        CentralModule.main.PlayArea.LoseCard(this);
        Costs.UpdateHealth(0);
        Burning.SetBurnIntencity(200, () =>
        {
            myRenderer.enabled = false;
            myUI.gameObject.SetActive(false);
            Burning.SetBurnIntencity(0, () =>
            {
                gameObject.SetActive(false);
                DiscadCard(this);
                myRenderer.enabled = true;
                myUI.gameObject.SetActive(true);

            },1);
        });
    }



    // Update is called once per frame



    static float PositionTransferVal(float t) { return t; }
    static float RotationTransferVal(float t)
    {
        return (t * t + (1 - (1 - t) * (1 - t))) / 2;
    }

    bool InPlay { get { return CentralModule.main.PlayArea.IsCardInPlay(this); } }
    public bool EnoughResources { get { return CentralModule.main.EnergyPool.CanTap(RedCost,GreenCost, BlueCost); }  }
    public bool EnoughToAct { get { return CentralModule.main.EnergyPool.CanTap(RedActivation, GreenActivation, BlueActivation); } }
    bool IsInspected;
    public void OnClicked()
    {
        IsInspected = true;
        OverrideCardPos = true;

        SetCardGraphicPos(CentralModule.main.InspectionPos.position, CentralModule.main.InspectionPos.forward * -1);
        //TargetCardViewPosition = CentralModule.main.InspectionPos.position;
        //TargetCardViewRotation = CentralModule.main.InspectionPos.position.rotation;

        //if (InPlay && EnoughResources)
        //{
        //    //We can do something here;
        //}
    }

    bool inHand { get { return CentralModule.main.Hand.IsCardInHand(this); } }
    public bool OnDragStart()
    {
        if (inHand)
        {
            if (OnCardDragStart != null) OnDragStart();
            ActiveCard = this;
            CentralModule.main.SetCardDragModeOn();
            IsPlayingCard = true;
            return true;
        }
        if (InPlay)
        {
            if (EnoughToAct)
            {
                CentralModule.main.StartTargetMode(EffectType);
                CentralModule.main.EnergyPool.Tap(RedActivation, GreenActivation, BlueActivation, transform.position);
                IsActivatingCard = true;
                return true;
            }
        }
        return false;
    }

    static Vector3 TablePosition;
    static bool TaplePosSet;
    static bool HoverOverPlayarea;
    static bool HoverOverBurnArea;

    HarmableTableObject ActiveTarget;
    bool IsPlayingCard, IsActivatingCard;

    public void OnDrag(RaycastHit HitInfo, bool isHit)
    {
        if (IsPlayingCard)
        {
            HoverOverBurnArea = false;
            HoverOverPlayarea = false;
            CentralModule.main.EnergyPool.Tap(0, 0, 0, Vector3.zero);
            if (isHit)
            {
                if (HitInfo.collider.GetComponentInParent<PlayArea>() != null)
                {
                    if (EnoughResources)
                    {
                        HoverOverPlayarea = true;
                        TablePosition = HitInfo.point;
                        SetCardGraphicPos(HitInfo.point, HitInfo.normal);
                        CentralModule.main.EnergyPool.Tap(RedCost, GreenCost, BlueCost, CardViewPosition.transform.position);
                    }
                }
                else if (HitInfo.collider.GetComponentInParent<SacrificeSpot>() != null)
                {
                    HoverOverBurnArea = true;
                    //SacrificeSpot.main.SetBurnIntencity(100);
                    SetCardGraphicPos(SacrificeSpot.main.CardBurnPosition.position, new Vector3(Mathf.Sin(Time.time * 10) / 2, 1, 0));
                }
            }
            else
            {
                ResetCardGraphicPos();
            }
        }
        if (IsActivatingCard)
        {
            Debug.Log("Targeting... " + isHit);
            if (isHit)
            {
                ActiveTarget = HitInfo.collider.GetComponentInParent<HarmableTableObject>();

                if (ActiveTarget != null)
                {
                    //Aim Arrow at the target!
                }
                else
                {
                    // Aim arrow at HitInfo.point;
                }
            }
        }
    }


    public void OnDragConfirm()
    {
        Debug.Log("DropConfirmed");
        if (IsPlayingCard)
        {
            CentralModule.main.ExitCardDragMode();
            OverrideCardPos = false;

            if (HoverOverPlayarea)
            {
                Ray seekTable = new Ray(TablePosition, Vector3.down);
                RaycastHit hitInfo;
                if (Physics.Raycast(seekTable, out hitInfo, 999, LayerMask.GetMask("Table")))
                {
                    SetCardPos(hitInfo.point, Quaternion.LookRotation(hitInfo.normal * -1, Camera.main.transform.forward));
                    CentralModule.main.EnergyPool.UseTapped();
                    Costs.ShowHealth();
                    CentralModule.main.PlayArea.AddCard(this);
                }
                Hand.main.LoseCard(this);
            }
            else if (HoverOverBurnArea)
            {
                CentralModule.main.StartAnimation();
                Hand.main.LoseCard(this);
                SetCardPos(SacrificeSpot.main.transform.position, Quaternion.LookRotation(Vector3.down, Camera.main.transform.forward));
                SacrificeSpot.main.SetBurnIntencity(200, () =>
                {
                    SacrificeSpot.main.SetBurnIntencity(0);
                });
                OnPositionReached += () =>
                {

                    //Avard Mana here;
                    //Debug.Log("Award some mana");

                    CentralModule.main.EnergyPool.Award(RedSalvade, GreenSalvage, BlueSalvage);

                    gameObject.SetActive(false);

                    DiscadCard(this);
                    CentralModule.main.StopAnimation();

                };
                //Burn the card for resources...
            }
            IsPlayingCard = false;
        }
        if (IsActivatingCard)
        {
            CentralModule.main.ExitTargetMode();
            Debug.Log("Aiming complete. " + (ActiveTarget == null ?"NoTraget": "TargetValid = "+ActiveTarget.IsValidTarget));
            if (ActiveTarget!=null && ActiveTarget.IsValidTarget)
            {
                CentralModule.main.EnergyPool.UseTapped();
                CentralModule.main.StartAnimation();
                Debug.Log("Need to PewPew at an enemy!");
                CentralModule.main.ProjectileManager.GetProjectile(EffectType == -1, EffectType).Launch(transform.position + Vector3.up * 0.5f, ActiveTarget.transform.position, () =>
                {
                    if (EffectType != -1)
                    {
                        ActiveTarget.Damage(EffectStrength);
                    }
                    else
                    {
                        ActiveTarget.Repair(EffectStrength);
                    }
                    CentralModule.main.StopAnimation();
                });
            }
            else
            {
                CentralModule.main.EnergyPool.Tap(0, 0, 0, Vector3.zero);
            }
            //HideArrow,
            IsActivatingCard = false;
        }
    }



    public void OnDragCancel()
    {
        if (IsPlayingCard)
        {
            CentralModule.main.ExitCardDragMode();
            ResetCardGraphicPos();
        }
        if (IsActivatingCard)
        {
            //HideArrow, Untap energy;
            CentralModule.main.ExitTargetMode();
            CentralModule.main.EnergyPool.Tap(0, 0, 0, Vector3.zero);
        }
    }
    bool HoverState;
    event System.Action OnHoverStateChanged;

    public void OnMouseEnter()
    {
        if (inHand)
        {
            HighlightColor = EnoughResources ? Color.green : Color.red;
            //HoverState = true;
            //if (OnHoverStateChanged != null) OnHoverStateChanged();
        }
        if (InPlay)
        {
            HighlightColor = EnoughToAct ? Color.green : Color.red;
        }
    }

    public void OnMouseExit()
    {
        HighlightColor = new Color(0,0,0,0);
        if (IsInspected)
        {
            OverrideCardPos = false;
        }
        //HoverState = false;
        //if (OnHoverStateChanged != null) OnHoverStateChanged();
    }

    class CardPiles
    {
        List<Card> A = new List<Card>();
        List<Card> B = new List<Card>();
        bool DrawFromB = false;
        public Card Draw()
        {
            if (A.Count == 0) DrawFromB = true;
            if (B.Count == 0) DrawFromB = false;
            return DrawFromList(DrawFromB ? B : A);
        }
        Card DrawFromList(List<Card> List)
        {
            if (List.Count == 0) return null;
            int I = Random.Range(0, List.Count);
            Card res = List[I];
            List.RemoveAt(I);
            return res;
        }
        public void Discard(Card C)
        {
            if (C == null) return;
            if (DrawFromB) A.Add(C);
            else B.Add(C);
        }
    }
}
