﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Card))]
public class HardHoverManager : MonoBehaviour, MouseManager.IHover
{
    public event System.Action<bool> OnHoverStateChange;
    Card _myCard;
    Card MyCard {
        get
        {
            if (_myCard == null) _myCard = GetComponent<Card>();
            return _myCard;
        }
    }

    public void OnMouseEnter()
    {
        if (OnHoverStateChange!= null) OnHoverStateChange(true) ;
    }

    public void OnMouseExit()
    {
        if (OnHoverStateChange != null) OnHoverStateChange(false);
    }

}
