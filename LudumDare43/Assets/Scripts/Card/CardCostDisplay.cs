﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIText = UnityEngine.UI.Text;

public class CardCostDisplay : MonoBehaviour {
    public GameObject Red,Green,Blue;
    // Use this for initialization
    protected int red,green,blue;
    protected UIText RTxt, GTxt, BTxt;
    bool wasInit = false;
    void Init()
    {
        if (!wasInit)
        {
            RTxt = Red.GetComponentInChildren<UIText>();
            if (RTxt.GetComponent<UITextMirror>() != null) RTxt = RTxt.GetComponent<UITextMirror>().baseText;
            GTxt = Green.GetComponentInChildren<UIText>();
            if (GTxt.GetComponent<UITextMirror>() != null) GTxt = GTxt.GetComponent<UITextMirror>().baseText;
            BTxt = Blue.GetComponentInChildren<UIText>();
            if (BTxt.GetComponent<UITextMirror>() != null) BTxt = BTxt.GetComponent<UITextMirror>().baseText;
            wasInit = true;
        }
    }
    public void Display(int Red, int Green, int Blue)
    {
        if (!wasInit) Init();
        this.Red.SetActive(Red > 0);
        RTxt.text = ""+Red;
        red = Red;
        this.Blue.SetActive(Blue > 0);
        BTxt.text = ""+Blue;
        blue = Blue;
        this.Green.SetActive(Green > 0);
        GTxt.text = ""+Green;
        green = Green;
    }
}
