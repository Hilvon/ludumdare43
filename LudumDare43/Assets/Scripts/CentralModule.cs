﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralModule : MonoBehaviour {
    public static CentralModule main;

    public MouseManager InputModule;
    public PlayArea PlayArea;
    public SacrificeSpot SacrificeSpot;
    public EnergyPool EnergyPool;
    public Hand Hand;
    public Transform InspectionPos;
    public ProjectileManager ProjectileManager;

    public enum GameMode { StartAction, DragCard, DragTarget, WaitAnimation}
    public GameMode CurMode;// { get; private set; }
    
    public HarmableTableObject PlayerHealth;
    public HarmableTableObject EnemyHealth;

    public UIHEalthDisplay PlayerHealthbar;
    public UIHEalthDisplay EnemyHealthbar;

    public Transform CardSpawnPos;
    public Card CardTemlate;
    public Hex HexTemplate;

    public void SetCardDragModeOn()
    {
        if (CurMode == GameMode.StartAction) {
            PlayArea.HighlightColor = Card.ActiveCard.EnoughResources ? Color.green : Color.clear;
            SacrificeSpot.HighlightColor = Color.red;
            SacrificeSpot.SetBurnIntencity(10);

            InputModule.InteractableLayers = LayerMask.GetMask("CardDrop");

            CurMode = GameMode.DragCard;
        }
    }
    public void ExitCardDragMode()
    {
        if (CurMode == GameMode.DragCard)
        {
            StartWaitAction();
        }
    }
    public void StartTargetMode(int Type)
    {
        if (CurMode == GameMode.StartAction)
        {
            if (Type != -1)
            {
                InputModule.InteractableLayers = LayerMask.GetMask("Hexes", "Table");
                Hex.HighlightTargetHex(Type);
            }
            else
            {
                PlayArea.HighlightHealTargets();
                InputModule.InteractableLayers = LayerMask.GetMask("CardInPlay", "Table");

            }
            CurMode = GameMode.DragTarget;
        }
    }
    public void ExitTargetMode()
    {
        if (CurMode == GameMode.DragTarget)
        {
            StartWaitAction();
        }
    }
    public void StartAnimation()
    {
        InputModule.InteractableLayers = 0;
        CurMode = GameMode.WaitAnimation;
    }
    public void StopAnimation()
    {
        if (CurMode == GameMode.WaitAnimation)
        {
            StartWaitAction();
        }
    }

    void StartWaitAction()
    {
        PlayArea.HighlightColor = Color.clear;
        SacrificeSpot.HighlightColor = Color.clear;
        SacrificeSpot.SetBurnIntencity(0);

        InputModule.InteractableLayers = LayerMask.GetMask("CardsInHand", "CardInPlay", "Hexes");
        CurMode = GameMode.StartAction;
    }
    Stack<System.Action<System.Action>> HexMoveStack = new Stack<System.Action<System.Action>>();
    public void NextTurn()
    {

        StartAnimation();
        Hex.IterateHexes((H) =>
        {
            Debug.Log("Scheduling Hex Action: " + H.name);
            HexMoveStack.Push(H.RunAction);
        });
        EnergyPool.BurnMana(() => {
            NextAction();
        });
    }

    void NextAction()
    {
        if (HexMoveStack.Count > 0)
        {
            Timer = 0;
            HexMoveStack.Pop()(() => { NextStep = NextAction; });
        }
        else
        {
            List<Card> NewCards = new List<Card>();

            for (int i = Hand.HeldCards.Length; i < 6; i++)
            {
                Card C = Card.DrawCard();
                C.Initialize();
                C.gameObject.SetActive(true);
                C.EnforceCardGraphics(CardSpawnPos.position, CardSpawnPos.rotation);
                NewCards.Add(C);
            }
            Hand.AddCards(NewCards.ToArray());
            Debug.Log("AllActions Executed.");
            StopAnimation();
        }
    }

    void Start()
    {
        if (main == null) main = this;
        PlayerHealthbar.Initialized(PlayerHealth.MaxHP);
        PlayerHealth.OnHPChanged += PlayerHealthbar.UpdateHealthValue;
        PlayerHealth.Initialize();
        PlayerHealth.OnDead += LoseGame;

        EnemyHealthbar.Initialized(EnemyHealth.MaxHP);
        EnemyHealth.OnHPChanged += EnemyHealthbar.UpdateHealthValue;
        EnemyHealth.Initialize();
        EnemyHealth.OnDead += WinGame;
        using (System.IO.MemoryStream ms = new System.IO.MemoryStream(CardSpecs.bytes))
        {
            using (System.IO.StreamReader SR = new System.IO.StreamReader(ms))
            {
                List<string> specs = new List<string>();
                while (!SR.EndOfStream)
                {
                    specs.Add(SR.ReadLine());
                }
                LoadCards(specs.ToArray());
            }
        }

        List<Card> FirstDraw = new List<Card>();

        for (int i = 0; i < 6; i++)
        {
            Card C = Card.DrawCard();
            C.Initialize();
            C.gameObject.SetActive(true);
            C.EnforceCardGraphics(CardSpawnPos.position, CardSpawnPos.rotation);
            FirstDraw.Add(C);
        }
        Hand.AddCards(FirstDraw.ToArray());
    }
    System.Action NextStep;
    float Timer;
    private void Update()
    {
        if (NextStep != null)
        {
            Timer -= Time.deltaTime;
            if (Timer<=0)
            {
                System.Action tmp = NextStep;
                NextStep = null;
                tmp();
            }
        }
    }

    public void LoadCards( string[] Lines)
    {
        foreach (string l in Lines)
        {
            string[] bits = l.Split('\t', ',',';');
            if (bits.Length>12)
            {
                Card newCard = Instantiate(CardTemlate, transform);
                newCard.Title = bits[0].Trim ();
                if (!int.TryParse(bits[1], out newCard.RedSalvade)) newCard.RedSalvade = 0;
                if (!int.TryParse(bits[2], out newCard.GreenSalvage)) newCard.GreenSalvage = 0;
                if (!int.TryParse(bits[3], out newCard.BlueSalvage)) newCard.BlueSalvage = 0;

                if (!int.TryParse(bits[4], out newCard.RedCost)) newCard.RedCost = 0;
                if (!int.TryParse(bits[5], out newCard.GreenCost)) newCard.GreenCost = 0;
                if (!int.TryParse(bits[6], out newCard.BlueCost)) newCard.BlueCost = 0;

                if (!int.TryParse(bits[7], out newCard.RedActivation)) newCard.RedActivation = 0;
                if (!int.TryParse(bits[8], out newCard.GreenActivation)) newCard.GreenActivation = 0;
                if (!int.TryParse(bits[9], out newCard.BlueActivation)) newCard.BlueActivation = 0;

                if (!int.TryParse(bits[10], out newCard.EffectType)) newCard.EffectType = 0;
                if (!int.TryParse(bits[11], out newCard.EffectStrength)) newCard.EffectStrength = 1;

                if (!int.TryParse(bits[12], out newCard.MaxHP)) newCard.MaxHP = 1;
                if (!int.TryParse(bits[13], out newCard.HPType)) newCard.HPType = 0;
                newCard.IsDefender = (bits[14] == "v");


                newCard.UpdateDisplays();
                newCard.gameObject.SetActive(false);
                Card.DiscadCard(newCard);
            }
        }
    }

    public TextAsset CardSpecs;
    public void Restart() {
        Hex.ClearList();
        Card.Clear();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    public CanvasGroup WinScreen, LoseScreen;
    public void LoseGame() { LoseScreen.gameObject.SetActive(true); LoseScreen.alpha = 1; LoseScreen.interactable = true; }
    public void WinGame() { WinScreen.gameObject.SetActive(true);  WinScreen.alpha = 1; WinScreen.interactable = true; }
}
