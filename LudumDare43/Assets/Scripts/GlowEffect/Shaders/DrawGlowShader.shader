﻿Shader "GlowEffect/DrawGlowShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
        _GlowTex ("GlowsImage",2D) = "Black" {}
        _ScreenH ("ScheenHeight", int) = 768
        _ScreenW ("ScreenWidth", int) = 1024
        _GlowWidth ("GlowWidth", int) = 3
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
            sampler2D _GlowTex;
            float _GlowWidth;
            float _ScreenH;
            float _ScreenW;
            
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 gcol = tex2D(_GlowTex, i.uv);
                fixed4 Gcol = fixed4(0,0,0,0);
                float vOffset = _GlowWidth / (_ScreenH);
                float hOffset = _GlowWidth / (_ScreenW);
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-3,vOffset*-3) ) * 0.000036;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 3,vOffset*-3) ) * 0.000036;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-3,vOffset* 3) ) * 0.000036;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 3,vOffset* 3) ) * 0.000036;
                
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-2,vOffset*-3) ) * 0.000363;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 2,vOffset*-3) ) * 0.000363;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-3,vOffset*-2) ) * 0.000363;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 3,vOffset*-2) ) * 0.000363;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-2,vOffset* 3) ) * 0.000363;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 2,vOffset* 3) ) * 0.000363;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-3,vOffset* 2) ) * 0.000363;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 3,vOffset* 2) ) * 0.000363;
                
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-1,vOffset*-3) ) * 0.001446;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-3,vOffset*-1) ) * 0.001446;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 1,vOffset*-3) ) * 0.001446;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 3,vOffset*-1) ) * 0.001446;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-1,vOffset* 3) ) * 0.001446;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-3,vOffset* 1) ) * 0.001446;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 1,vOffset* 3) ) * 0.001446;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 3,vOffset* 1) ) * 0.001446;
                
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 0,vOffset*-3) ) * 0.002291;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-3,vOffset* 0) ) * 0.002291;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 0,vOffset* 3) ) * 0.002291;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 3,vOffset* 0) ) * 0.002291;
                
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-2,vOffset*-2) ) * 0.003676;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 2,vOffset*-2) ) * 0.003676;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-2,vOffset* 2) ) * 0.003676;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 2,vOffset* 2) ) * 0.003676;
                
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-1,vOffset*-2) ) * 0.014662;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 1,vOffset*-2) ) * 0.014662;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-2,vOffset*-1) ) * 0.014662;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 2,vOffset*-1) ) * 0.014662;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-1,vOffset* 2) ) * 0.014662;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 1,vOffset* 2) ) * 0.014662;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-2,vOffset* 1) ) * 0.014662;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 2,vOffset* 1) ) * 0.014662;
                
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 0,vOffset*-2) ) * 0.023226;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-2,vOffset* 0) ) * 0.023226;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 0,vOffset* 2) ) * 0.023226;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 2,vOffset* 0) ) * 0.023226;
                
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-1,vOffset*-1) ) * 0.058488;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 1,vOffset*-1) ) * 0.058488;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-1,vOffset* 1) ) * 0.058488;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 1,vOffset* 1) ) * 0.058488;
                
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 0,vOffset*-1) ) * 0.092651;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 0,vOffset* 1) ) * 0.092651;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset*-1,vOffset* 0) ) * 0.092651;
                Gcol += tex2D(_GlowTex, i.uv + float2(hOffset* 1,vOffset* 0) ) * 0.092651;
                
                Gcol += gcol * 0.146768;
                
                
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-2,hOffset*-2) )*1;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-1,hOffset*-2) )*4;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 0,hOffset*-2) )*7;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 1,hOffset*-2) )*4;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 2,hOffset*-2) )*1;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-2,hOffset*-1) )*4;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-1,hOffset*-1) )*16;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 0,hOffset*-1) )*26;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 1,hOffset*-1) )*16;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 2,hOffset*-1) )*4;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-2,hOffset* 0) )*7;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-1,hOffset* 0) )*26;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 1,hOffset* 0) )*26;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 2,hOffset* 0) )*7;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-2,hOffset* 1) )*4;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-1,hOffset* 1) )*16;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 0,hOffset* 1) )*26;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 1,hOffset* 1) )*16;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 2,hOffset* 1) )*4;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-2,hOffset* 2) )*1;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset*-1,hOffset* 2) )*4;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 0,hOffset* 2) )*7;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 1,hOffset* 2) )*4;
                //Gcol += tex2D(_GlowTex, i.uv + float2(vOffset* 2,hOffset* 2) )*1;
				// just invert the colors
                //Gcol = Gcol / 273;
                
				col = lerp(lerp(col,Gcol,1-((1-Gcol.a)*(1-Gcol.a)*(1-Gcol.a))),col,gcol.r != 0 || gcol.g != 0 || gcol.b !=0);
				return col;
			}
			ENDCG
		}
	}
}
