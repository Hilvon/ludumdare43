﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowEffectOverlay : MonoBehaviour {

    RenderTexture myTexture;
    Camera myCamera;
    public Material GlowOverlayMaterial;
    public Shader GlowRenderShader;
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, GlowOverlayMaterial);
    }
    public LayerMask GlowSourcesMask;
    // Use this for initialization
    void Start () {
        myCamera = new GameObject("GlowCamera", typeof(Camera)).GetComponent<Camera>();
        myCamera.transform.SetParent(transform, false);
        myCamera.SetReplacementShader(GlowRenderShader,"");
        myCamera.clearFlags = CameraClearFlags.SolidColor;
        myCamera.backgroundColor = new Color(0,0,0,0);
        SetTextureSize(Screen.width, Screen.height);
        myCamera.cullingMask = GlowSourcesMask;
    }

    void SetTextureSize(int Width, int Height)
    {
        Debug.Log("Setting texture size");
        myTexture = new RenderTexture(Width, Height, 1);
        myCamera.targetTexture = myTexture;
        GlowOverlayMaterial.SetTexture("_GlowTex", myTexture);
        GlowOverlayMaterial.SetInt("_ScreenW", Width);
        GlowOverlayMaterial.SetInt("_ScreenH", Height);
    }
    Vector2 lastScreenSize;
    // Update is called once per frame
    void Update () {
        Vector2 curScreen = new Vector2(Screen.width, Screen.height);
        if (Vector2.Distance (lastScreenSize,curScreen)>1)
        {
            SetTextureSize(Screen.width, Screen.height);
            lastScreenSize = curScreen;
        }
    }
}
