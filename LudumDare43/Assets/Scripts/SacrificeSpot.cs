﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SacrificeSpot : MonoBehaviour {
    public static SacrificeSpot main;
    public ParticleSystem myParticleSystem;

    public Transform CardBurnPosition;
	// Use this for initialization
	void Start () {
        myParticleSystem = GetComponentInChildren<ParticleSystem>();
        curEmission = myParticleSystem.emission;
        curMainParticles = myParticleSystem.main;
        SetBurnIntencity(10);
        if (main == null) main = this;

        myRenderer = GetComponentInChildren<MeshRenderer>();
        myPRP = new MaterialPropertyBlock();
    }
    System.Action OnIntencityChanged;
    int TargetIntencity;
	public void SetBurnIntencity(int N, System.Action Callback = null)
    {
        TargetIntencity = N;
        if (Callback != null) OnIntencityChanged += Callback;
    }

    ParticleSystem.EmissionModule curEmission;
    ParticleSystem.MainModule curMainParticles;
    // Update is called once per frame
    void Update () {
        float curRate = curEmission.rateOverTime.constant;
        curRate = Mathf.Lerp(curRate, TargetIntencity, 2 * Time.deltaTime);

        curEmission.rateOverTime = new ParticleSystem.MinMaxCurve(curRate);
        curMainParticles.startSize = Mathf.Log(curRate + 1,10);
        if (Mathf.Abs(curRate-TargetIntencity)<1 && OnIntencityChanged != null)
        {
            OnIntencityChanged();
            OnIntencityChanged = null;
        }

    }

    MaterialPropertyBlock myPRP;
    public Color HighlightColor
    {
        get { return myPRP.GetColor("_GlowColor"); }
        set
        {
            myPRP.SetColor("_GlowColor", value);
            myRenderer.SetPropertyBlock(myPRP);
        }
    }
    MeshRenderer myRenderer;
    // Use this for initialization

}
