﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIText = UnityEngine.UI.Text;

public class HexTimeDisplay : MonoBehaviour {
    public UIText CurStepDisplay;
    public UIText CooldownInfoDisplay;

    public void InitDisplay(int Cooldown, int FirstCooldown)
    {
        CooldownInfoDisplay.text = Cooldown +" (" + FirstCooldown + ")";
        CurStepDisplay.text = FirstCooldown+"";
    }
    public void UpdateCurStep(int CurStep)
    {
        CurStepDisplay.text = "" + CurStep;
    }
}
