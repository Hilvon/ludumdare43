﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIText = UnityEngine.UI.Text;
using UIImage = UnityEngine.UI.Image;

public class HealthBar : CardCostDisplay {
    public UIImage HPType;
    public UIImage Bar;
    public UIImage VBar;
    public UIText HPText;
    public Sprite ShieldSprite;
    public Sprite SoftSprite;
    // Use this for initialization
    public void ShowHealth()
    {
        Red.SetActive(false);
        Green.SetActive(false);
        Blue.SetActive(false);
        Bar.gameObject.SetActive(true);
    }
    public void ShowCosts()
    {
        Red.SetActive(red>0);
        Green.SetActive(green>0);
        Blue.SetActive(blue>0);
        Bar.gameObject.SetActive(false);
    }
    float MaxHealth;
    public void InitHealth (int Max, int Type, bool IsShield)
    {
        MaxHealth = Max;

        HPType.color = GetColorPyType(Type);
        HPType.sprite = IsShield ? ShieldSprite : SoftSprite;
        HPText.text = "" + Max;
    }

    public static Color GetColorPyType(int type)
    {
        switch(type)
        {
            case 0: return Color.cyan;
            case 1: return Color.magenta;
            case 2: return Color.yellow;
            default:return Color.clear;
        }
    }
    public void UpdateHealth(int cur)
    {
        VBar.fillAmount = cur / MaxHealth;
        HPText.text = (cur < MaxHealth) ? (cur + "/" + MaxHealth) : "" + MaxHealth;
    }
}
