﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarmableTableObject : MonoBehaviour {
    public bool IsValidTarget { get; set; }

    public event System.Action OnDead;
    public event System.Action<int> OnHPChanged;
    public int MaxHP;
    int curHP;
    public int HPType;
    public bool IsDefender;
	

    public bool IsMaxHP()
    {

        return curHP == MaxHP;
    }


    public virtual void Initialize()
    {
        curHP = MaxHP;
        UpdateHP();
    }
    public void Damage(int Amount)
    {
        Debug.Log("Receiving damage - " + Amount +"; currentHP: "+curHP );
        curHP -= Amount;

        if (curHP <= 0) { if (OnDead != null) OnDead(); }
        else
        {
            UpdateHP();
        }

    }
    public void Repair(int Amount)
    {
        curHP += Amount;
        if (curHP > MaxHP) curHP = MaxHP;
        UpdateHP();
    }
    void UpdateHP()
    {
        if (OnHPChanged != null) OnHPChanged(curHP);
    }

    MaterialPropertyBlock myPRP;
    public Color HighlightColor
    {
        get { return myPRP.GetColor("_GlowColor"); }
        set
        {
            myPRP.SetColor("_GlowColor", value);
            myRenderer.SetPropertyBlock(myPRP);
        }
    }
    protected MeshRenderer myRenderer;
    protected Canvas myUI;
    protected bool OverrideCardPos;
    private Vector3 TargetCardViewPosition;
    private Quaternion TargetCardViewRotation;

    public Transform CardViewPosition;

    protected virtual void Start()
    {
        myRenderer = GetComponentInChildren<MeshRenderer>();
        myUI = GetComponentInChildren<Canvas>();
        myPRP = new MaterialPropertyBlock();

        //Initialize();
    }

    public void SetCardPos(Vector3 Pos, Quaternion Rot)
    {
        Vector3 cPos = CardViewPosition.position;
        Quaternion cRot = CardViewPosition.rotation;

        transform.position = Pos;
        transform.rotation = Rot;

        CardViewPosition.position = cPos;
        CardViewPosition.rotation = cRot;
    }

    protected void SetCardGraphicPos(Vector3 Position, Vector3 Normal)
    {
        TargetCardViewPosition = Position;
        TargetCardViewRotation = Quaternion.LookRotation(Normal * -1, Camera.main.transform.forward);
        OverrideCardPos = true;
    }
    protected void ResetCardGraphicPos()
    {
        OverrideCardPos = false;
        //TargetCardViewPosition = transform.position;
        //TargetCardViewRotation = transform.rotation;
    }
    public event System.Action OnPositionReached;
    public void EnforceCardGraphics(Vector3 position, Quaternion Rotation)
    {
        CardViewPosition.position = position;
        CardViewPosition.rotation = Rotation;
    }
    void Update()
    {
        //HighlightColor = TestColor;
        Vector3 RequiredPosition = OverrideCardPos ? TargetCardViewPosition : transform.position;
        CardViewPosition.position = Vector3.Lerp(CardViewPosition.position, RequiredPosition, 4 * Time.deltaTime);
        CardViewPosition.rotation = Quaternion.LerpUnclamped(CardViewPosition.rotation, OverrideCardPos ? TargetCardViewRotation : transform.rotation, 4 * Time.deltaTime);
        if (OnPositionReached != null && Vector3.Distance(RequiredPosition, CardViewPosition.position) < 0.5f)
        {
            OnPositionReached();
            OnPositionReached = null;
        }
    }
}
