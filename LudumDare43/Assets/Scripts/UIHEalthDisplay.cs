﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIText = UnityEngine.UI.Text;
using UIImage = UnityEngine.UI.Image;

public class UIHEalthDisplay : MonoBehaviour {
    public UIImage Healthbar;
    public UIText HealthNumber;

    float MaxVal;

    public void Initialized(int MaxValue)
    {
        HealthNumber.text = MaxValue + " / " + MaxValue;
        Healthbar.fillAmount = 1f;
        MaxVal = MaxValue;
    }
    public void UpdateHealthValue(int CurValue)
    {
        HealthNumber.text = CurValue + " / " + MaxVal;
        Healthbar.fillAmount = CurValue / MaxVal;
    }
}
